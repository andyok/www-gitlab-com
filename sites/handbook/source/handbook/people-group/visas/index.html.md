---
layout: handbook-page-toc
title: "Visas"
description: "Information on travel visas, visa letters, and immigration to the Netherlands. "
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Although this page is all about Visas and how to apply for them, kindly keep in mind that GitLab does not  offer any form of Work Sponsorship anywhere in the world. We do have guidance below on how we provide sponsorship in the [Netherlands](/handbook/people-group/visas/#right-to-immigrate-to-the-netherlands), but we do not offer this in any other location at this time. We are also not in a position to transfer existing Work permit's at this time.

## Expensing

You can expense:

- Cost of the VISA
- Cost of pictures for the VISA
- Cost of a lawyer to help you with the application
- Cost of postage
- Cost of applying for the [30% ruling](#30-ruling-in-the-netherlands)

You can _not_ expense relocation costs.

## Visa Contacts

For assistance with any employment or relocation visa please reach out to the People Ops Manager. Point of contact for Contribute and travel visas is TBD.

## Travel Visas

### Arranging a visa for travel

In some cases when traveling to a conference or summit you might need to arrange a visa to enter that specific country.

Always **check the website of the Embassy of the Country you want to visit** located in your home country. They will have the most updated information on whether or not you need a visa and what is needed for your application. In some instances you can travel under a visa waiver program (e.g. [ESTA](https://esta.cbp.dhs.gov/esta/)) in other cases you might need to arrange a tourist or business visa. Read their website carefully and ask People Ops Specialists if you need any help with acquiring the needed documents for your application.

We gladly organize and pay for local legal assistance, especially if you are at high risk of having a long process.

#### Tips & Tricks for visa arrangements <a name="visa-tips"></a>

- **Make sure to start on time with your arrangements for a visa**. It can take up to 3-4 **months** to acquire a US visa, as an example. Consider this recent example that several team members faced when they were arranging their visa to come to the summit in Austin in May of 2016. First, there was a delay of weeks to a month between the time they called to make an appointment at a US embassy and the date of the actual appointment. In some cases, the application then moved through the process with no issues and the visa was ready within two weeks. In other cases, the visa application was marked for "administrative processing" which can take up to 60 days, and is pretty much a [black hole in terms of information and process](https://www.quora.com/How-do-I-expedite-the-administrative-process-for-the-U-S-visa).
- Check if your passport is still valid long enough.
- Give priority to arranging your visa, it can take time to get an appointment to apply at the Embassy.
- Double check if you have all needed document and information. Ask People Ops Specialists to help you check this if you like.
- Think of the answers you will give during your visa interview; it can influence your visa approval/denial.
- Once the dates for a conference or summit are set start asap with your application if you have needed a
special visa in previous situations, just to give yourself enough time and room for errors if they arise.
- If required, you can request a Visa Invitation Letter following [this](/handbook/people-group/frequent-requests/#do-you-need-a-visa-invitation-letter) process

##### Timeline guide for visa applications

_Please note that the below timeline is just a guide, and you should always check the guidance on the official website of the embassy for the country you are applying to before making your application or traveling to an interview._

- **6 months beforehand**: make appointment at embassy, and start gathering all necessary paperwork, forms, certificates, pictures, etc.
   - Sometimes you might call the embassy, and be told that you do not need to schedule an interview so far ahead of your trip. Don't take their word for it, since that advice is typically made with the "typical, best case"  process in mind and for example in the case of the USA does not account for the possibility of a black hole "administrative processing" period. In other words, make sure you get an appointment as early as possible.
   - If the country where the embassy/consulate is located where you are going for your application is listed as anything other than "High Income" by the [World Bank's "Little Data Book"](https://openknowledge.worldbank.org/bitstream/handle/10986/23968/9781464808340.pdf?sequence=4&isAllowed=y) then it is likely that the embassy/consulate there has a [high tendency to say "no" to visa applications](http://www.alllaw.com/articles/nolo/us-immigration/harder-get-visa-from-certain-countries.html). We recommend to engage the services of a local lawyer who is familiar with the embassy process to help check that your paperwork is in order, and to practice interview questions. You're also welcome to ask for support when you are in a "High Income" country. People Ops can help you with this.
- **3 months beforehand** (or earlier if indicated by the embassy you are applying to): go to embassy for interview.
- **2 months beforehand**: if you have not received your visa by now, contact your local lawyer again, and/or People Ops, to see if anything can be done to expedite the process.

## Dutch Work Permits

Some of our GitLab team-members in the Netherlands have a "werkvergunning" or work permit under the [highly skilled migrants](https://ind.nl/en/work/working_in_the_Netherlands/Pages/Highly-skilled-migrant.aspx) category of the Immigration and Naturalization Service (IND).

- GitLab is a recognized organization ("erkend referent") with the IND, and Savvy provides support with respect to applying for new visas / permits or extending existing ones.
- Work permits must be renewed at the end of each contract period, but at minimum once every 5 years.
- At the time of applying for permit renewal, the application must satisfy various criteria including an age-dependent [minimum salary requirement](https://ind.nl/en/Pages/required-amounts-income-requirement.aspx) (with a step at age 30, also see this [handy table](https://ind.nl/en/Documents/Which_amount_applies_to_my_highly_skilled_migrant.pdf)). This requirement should be taken into consideration when issuing a new contract, since the contract can be made valid for just a year or for an indefinite period; thus triggering more or less frequent re-applications for work permit extensions. Permit extensions cannot be applied for when the current permit is still valid for more than 6 months.

Here is a [generally but not authoritative helpful guide](http://www.expatica.com/nl/visas-and-permits/When-your-residence-permit-expires-or-you-want-to-leave-the-Netherlands_108416.html) on considerations around permit extensions.

1. Some important notes before you arrange travel / depart to the Netherlands:
- **A direct flight to the Netherlands is required, indirect travel may result in entry refusal**

#### 30% Ruling in the Netherlands

The [30% reimbursement ruling](https://www.iamsterdam.com/en/living/take-care-of-official-matters/highly-skilled-migrants/thirty-percent-ruling) (better known as the 30% ruling) is a tax advantage for highly skilled migrants moving to the Netherlands. This benefit, granted for five years, allows them to receive 30% of their employment income tax free. As an example, when your employment income is EUR 60,000; the 30% ruling ensures a net pay of EUR 18,000. The remainder of EUR 42,000 is subject to Dutch taxes. In addition, when being entitled to the 30% ruling you can exclude your savings from Dutch income tax (which can be beneficial once you have exercised your stock options).

The 30% ruling is a mutual application filed by the employee and GitLab BV as the employer. During the onboarding procedure a [questionnaire](https://docs.google.com/document/d/1Ok6LS9T4P6tnPu2N6BDRDeveOYzd1ILpkbQRhl911w4/edit?ts=5caf1bca) is shared in order to gather the necessary information to file the application for the 30% ruling. People Ops shares the 30% ruling questionnaire and supporting documentation with  HRSavvy (the company that supports GitLab with visas and payroll in the Netherlands). HR Savvy will subsequently apply for the 30% ruling. There are some conditions to be satisfied to be granted the 30% ruling.

1. The employee has to be hired as an employee.
1. The employer and employee have to agree in writing that the 30% ruling is applicable (addendum to the employment agreement).
1. The employee has to transfer or to be recruited abroad by a Dutch employer. In two years before being hired by a Dutch employer, the employee must be living outside of the Netherlands for more than 16 months, at a minimal distance of 150 kilometers from the Dutch border.
1. The employee must have specific experience or expertise that is not or rarely available in the Netherlands.
1. The gross annual salary has to surpass a minimum (adjusted annually).

**The decision from the Dutch Tax Authorities can take up to four months.** Once your 30% ruling is granted, the application will be made retroactively in the payroll administration to your starting date. Read more from the Dutch tax authorities by clicking this [link](https://www.belastingdienst.nl/wps/wcm/connect/bldcontenten/belastingdienst/individuals/living_and_working/working_in_another_country_temporarily/you_are_coming_to_work_in_the_netherlands/30_facility_for_incoming_employees/).

Here is [an approximate net-salary calculator in The Netherlands](https://relocate.me/net-pay-calculators/the-netherlands) to reckon additional salary payment with 30% ruling.


#### Transferring the 30% Ruling from a Previous Employer

The 30% ruling is a tax advantage granted for five years, which means that you can carry this over to a new employer if your new role still fulfills the requirements of the 30% ruling. Note that you are only allowed to transfer the 30% ruling when there is a gap of maximum three months between your previous employment and your employment at GitLab BV. The 30% ruling questionnaire includes a section where you can state that you have already been entitled to the 30% ruling. Transferring the 30% ruling tends to be faster since your qualifications were already assessed at your previous employment.

#### BSN Number

A [BSN number](https://www.iamsterdam.com/en/living/take-care-of-official-matters/registration/citizen-service-number) is like a citizen number. It is required so new team members can be added to the B.V. Netherlands payroll. It's also required for things like health insurance and opening a bank account.

There are two options to get a BSN number in the Netherlands:

1. Getting a BSN number at the municipality. This process will take at least 1-4 weeks. There are no costs involved with registering and receiving a BSN at the municipality.

1. Getting a BSN number at your local [Expat center](https://www.iamexpat.nl/expat-info/organisations/expat-centres).
It is possible to make an appointment within 2 weeks.

### Right to Immigrate to the Netherlands

Everyone that meets the following requirements can request to immigrate and relocate to the Netherlands, but you will also still need to pass the formal visa application process to qualify. The three requirements are:

1. When using the [compensation calculator](/handbook/total-rewards/compensation/compensation-calculator/calculator/) you must meet the Dutch salary requirement for [highly skilled migrants for 3 more years](https://ind.nl/en/Pages/required-amounts-income-requirement.aspx#Application_for_residence_permit_highly_skilled_migrant_and_European_Blue_Card)
- Note, that the Dutch government has a higher requirement for team members aged 30 and above. The age related wage requirement does not increase when reaching 30 if you already have an approved migrant visa (with the same employer).
- The following pay elements are not included in the salary criterion and can't be used to meet the mimimum salary requirement: Vacation allowance; the value of payment made in kind; Uncertain, non-regular pay elements (for example overtime allowances, variable boni and payments from funds).
- _Note: This calculation should be based on what GitLab *would* pay the team member in the Netherlands in accordance with the compensation calculator, *not* based on the team member's current salary._
1. You have been a team member at GitLab for one year.
1. You are not on a Performance Improvement Plan (PIP).

#### Special considerations for team members in unsafe environments
Tenure of less than 1 year at GitLab may be possibly waived as a requirement if:
  - You are a member of a(n [underrepresented](https://about.gitlab.com/company/culture/inclusion/#definitions)) group that is unsafe or in any way mistreated in your country.
  - Violence in your country does not provide a safe environment.

If you meet these requirements, kindly read our [Relocation](https://about.gitlab.com/handbook/people-group/relocation/) handbook page, as well as our specific [Netherlands section](/handbook/people-group/relocation/#relocating-to-the-netherlands), to ensure that your request is evaluated and approved following our protocols. **Please email peopleops@ gitlab.com if you have any questions.**

If you do not meet one or more of the above requirements, please file a request with your manager and indicate clearly which requirements you meet and which ones you do not meet. All situations will be reviewed on a case by case basis. If you receive full manager approval, please forward that approval to peopleops @gitlab.com at the start of the process. **Manager approval to proceed does not ensure that you will be approved to relocate and immigrate.**

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/w_CRpuPwO0o" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Sid and Marin discuss the history of GitLab's right to immigrate to the Netherlands.
